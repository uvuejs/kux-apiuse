# kux-apiuse-storage
该插件是官方 [数据存储](https://doc.dcloud.net.cn/uni-app-x/api/storage.html) 的封装，支持单独设置有效期以及自动清除过期数据。

## 插件特色
+ 简洁高效
+ 支持设置有效期
+ 支持自动清除过期数据
+ 支持校验是否过期

## 基本用法

```ts
import { useStorage } from '@/uni_modules/kux-apiuse-storage';

const { setValue, setExpiry, isExpired, data, expiry, refresh } = useStorage<string>('test-version');

// 设置新值
setValue('test');
// 设置有效期，单位毫秒
setExpiry(3000);
// 获取是否过期
console.log(isExpired.value);
// 读取值
console.log(data.value);
// 读取过期时间
console.log(expiry.value);
// 刷新缓存状态
refresh();
```

## API
### useStorage
+ 说明：缓存操作函数
+ 类型：`useStorage<T>(key: string, extraOptions?: kuxUseStorageExtraOptions<T> | null): KuxUseStorageReturn<T>`

### useStorage 参数
| 名称 | 类型 | 必填 | 说明
| --- | --- | --- | ---
| key | `string` | 是 | 缓存key
| extraOptions | `kuxUseStorageExtraOptions<T>` | 否 | 额外配置参数

### kuxUseStorageExtraOptions 参数
| 名称 | 类型 | 必填 | 说明
| --- | --- | --- | ---
| initialValue | `T | null` | 否 | 默认值
| expiry | `number | null` | 否 | 有效期，单位毫秒
| clearExpired | `boolean` | 否 | 是否自动清除过期数据，默认为 `true`

### useStorage 返回值
#### getValue
+ 说明：读取缓存值
+ 类型：`() => T | null`

#### setValue
+ 说明：设置新值
+ 类型：`(newValue: T) => void`
+ 注意：如果设置为 `null` 就是清除缓存。

#### getExpiry
+ 说明：读取有效期
+ 类型：`() => number | null`

#### setExpiry
+ 说明：设置新的有效期
+ 类型：`(newExpiry: number | null) => void`

#### expiry
+ 说明：读取有效期，和 `getExpiry` 同样的效果。
+ 类型：`Ref<number | null>`

#### data
+ 说明：读取缓存值，和 `getValue` 同样的效果。
+ 类型：`Ref<T | null>`

#### isExpired
+ 说明：缓存是否过期，如果缓存数据不存在或者已过期，则返回 `true`，否则返回 `false`。
+ 类型：`Ref<boolean>`

#### refresh
+ 说明：刷新缓存状态，用来手动获取最新缓存状态
+ 类型：`() => void`

#### setItem
+ 说明：设置新的缓存，支持同时设置值和有效期
+ 类型：`(value: T, newExpiry?: number | null) => void`
+ 注意：如果设置为 `null` 就是清除缓存。

#### checkExpiry
+ 说明：校验是否过期，同 `isExpired` 效果。
+ 类型：`() => boolean`

#### clear
+ 说明：清除缓存
+ 类型：`() => void`

## 实战示例

```
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<view class="flex-col mt-3 px-3">
			<text>缓存值：{{ data }}</text>
			<text class="mt-3">缓存有效期：{{ expiryDate }}</text>
			<text class="mt-3">缓存是否过期：{{ isExpired ? '是' : '否' }}</text>
			<button class="mt-3" type="primary" @tap="setValue('test')">设置新值</button>
			<button class="mt-3" type="primary" @tap="setExpiry3Seconds">设置有效期3秒</button>
			<button class="mt-3" type="primary" @tap="refresh">刷新有效期</button>
		</view>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script setup>
	import { useStorage } from '@/uni_modules/kux-apiuse-storage';
	
	const { setValue, setExpiry, isExpired, data, expiry, refresh } = useStorage<string>('test-version', {
		clearExpired: true,
		initialValue: 'hello uts'
	});
	const expiryDate = computed(() => {
		if (expiry.value != null) {
			return new Date(expiry.value!);
		}
		
		return null;
	})
	
	const setExpiry3Seconds = () => {
		uni.showModal({
			title: '提示',
			content: '设置后3秒后数据过期',
			showCancel: false,
			success: (res) => {
				if (res.confirm) {
					setExpiry(3000);
					setTimeout(() => {
						refresh();
					}, 3000);
				}
			}
		})
	};
</script>

<style>
		
</style>
```

## 类型定义

```ts
/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type StorageWithExpiry<T> = {
	value: T;
	expiry: number | null;
}

export type KuxUseStorageReturn<T> = {
	getValue: () => T | null;
	setValue: (newValue: T) => void;
	getExpiry: () => number | null;
	setExpiry: (newExpiry: number | null) => void;
	expiry: Ref<number | null>;
	data: Ref<T | null>;
	isExpired: Ref<boolean>;
	refresh: () => void;
	setItem: (value: T, newExpiry?: number | null) => void;
}

export type kuxUseStorageExtraOptions<T> = {
	initialValue?: T | null;
	expiry?: number | null;
	clearExpired?: boolean;
}

export type KuxUseStorageOptions<T> = {
	key: string;
	initialValue?: T | null;
	expiry?: number | null;
	extraOptions?: kuxUseStorageExtraOptions | null;
}
```

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
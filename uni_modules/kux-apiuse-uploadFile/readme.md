# kux-apiuse-uploadFile
该插件是对官方 [uni.uploadFile](https://doc.dcloud.net.cn/uni-app-x/api/upload-file.html) 的异步版本封装，在支持原生API所有特性前提下可以使用异步hooks函数实现业务逻辑，使业务代码更简洁高效。

## 插件特色
+ 异步hooks函数风格
+ 简洁高效

## 基本用法

```ts
import { useUploadFile } from '@/uni_modules/kux-apiuse-uploadFile';

const { uploadFile } = useUploadFile();
const { result, fail, uploading } = await uploadFile({
	url: 'https://kuxdemo.uvuejs.cn/api/upload', // 非真实地址，仅作演示
	filePath: tempFilePaths[0], // 非真实变量，请替换自己真实的上传文件资源的路径
}, null);

if (fail != null) {
	console.log(`上传失败：${fail.errMsg}`); // 仅作演示，请根据自己实际逻辑处理上传失败
	return;
}
// 图片上传完成逻辑处理，仅作演示
url.value = tempFilePaths[0];
console.log(result); // 上传接口返回
```

### useUploadFile 返回参数
#### uploadFile
+ 说明：文件上传函数
+ 类型：`(options: UploadFileOptions, uploadTaskCallback?: ((task: UploadTask) => void) | null) => Promise<KuxUploadFileReturn>`
+ 参数说明：
	
	| 名称 | 类型 | 必填 | 说明
	| --- | --- | --- | ---
	| options | [UploadTask](https://doc.dcloud.net.cn/uni-app-x/api/upload-file.html#uploadtask-values) | 是 | 文件上传参数
	| uploadTaskCallback | `((task: UploadTask) => void) | null` | 是 | 上传任务监听回调，如果不需要回调直接写null即可

### UploadTask 说明
参考 [UploadTask](https://doc.dcloud.net.cn/uni-app-x/api/upload-file.html#uploadtask-values)

### KuxUploadFileReturn 返回参数
#### result
+ 说明：上传完成接口返回结果
+ 类型：[`UploadFileSuccess`](https://doc.dcloud.net.cn/uni-app-x/api/upload-file.html#uploadfilesuccess-values)

#### fail
+ 说明：上传失败结果
+ 类型：[`UploadFileFail`](https://doc.dcloud.net.cn/uni-app-x/api/upload-file.html#uploadfilefail-values)

#### uploading
+ 说明：是否正在上传
+ 类型：`boolean`

## 完整示例
```html
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<view class="flex justify-center items-center px-3 py-3 bg-white rounded-sm" @tap="onChooseImage">
			<text v-if="url == ''" class="text-primary-5">+ 选择图片</text>
			<image v-else :src="url" mode="widthFix"></image>
		</view>
		<view class="mt-3 px-3">
			<view>上传进度：{{ progress }}%</view>
			<view class="mt-3">已上传长度：{{ totalBytesSent }} Bytes</view>
			<view class="mt-3">总长度：{{ totalBytesExpectedToSend }} Bytes</view>
		</view>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script setup>
	import { useUploadFile } from '@/uni_modules/kux-apiuse-uploadFile';
	import { useChooseImage } from '@/uni_modules/kux-apiuse-chooseImage';
	
	const url = ref('');
	const progress = ref(0);
	const totalBytesSent = ref(0);
	const totalBytesExpectedToSend = ref(0);
	let uploadTask: UploadTask | null = null;
	
	const { uploadFile } = useUploadFile();
	const { chooseImage } = useChooseImage();
	
	const onChooseImage = async () => {
		const { error, tempFilePaths, tempFiles } = await chooseImage(null);
		if (error != null) {
			console.log(error);
			return;
		}
		const { result, fail } = await uploadFile({
			url: 'http://kuxdemo.uvuejs.cn/api/upload',
			filePath: tempFilePaths[0],
			name: 'image'
		}, (task) => {
			uploadTask = task;
			// 上传进度监听
			uploadTask!.onProgressUpdate((result) => {
				console.log(`当前进度：${result.progress}%，总长度：${result.totalBytesExpectedToSend}，已上传长度：${result.totalBytesSent}`);
				progress.value = result.progress;
				totalBytesSent.value = result.totalBytesSent;
				totalBytesExpectedToSend.value = result.totalBytesExpectedToSend;
			})
		})
		if (fail != null) {
			console.log(fail);
			return;
		}
		// 图片上传成功逻辑处理，仅作演示
		url.value = tempFilePaths[0];
		console.log(result);
	}
	
	// 页面卸载时中断上传
	onUnload(() => {
		uploadTask?.abort();
	})
</script>

<style>
		
</style>
```

> 注意
> 
> 示例中的 `kux-apiuse-chooseImage` 为单独的插件，需要在插件市场下载，[插件地址](https://ext.dcloud.net.cn/plugin?id=21014)

## 类型定义
```ts
/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type KuxUploadFileReturn = {
	result: UploadFileSuccess | null,
	fail: UploadFileFail | null,
	uploading: Ref<boolean>
}

export type KuxUseUploadFileReturn = {
	uploadFile: (options: UploadFileOptions, uploadTaskCallback?: ((task: UploadTask) => void) | null) => Promise<KuxUploadFileReturn>
}
```

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
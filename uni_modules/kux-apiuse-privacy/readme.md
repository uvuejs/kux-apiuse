# kux-apiuse-privacy
该插件是对官方 [隐私API](https://doc.dcloud.net.cn/uni-app-x/api/privacy.html) 的异步版本封装，在支持原生API所有特性前提下可以使用异步hooks函数实现业务逻辑，使业务代码更简洁高效。

## 插件特色
+ 异步hooks函数风格
+ 简洁高效

## 基本用法
+ 隐私状态管理页面

```html
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<view class="px-3 py-3">
			<view class="flex flex-row justify-between">
				<text class="text-lg">当前应用隐私授权状态:</text>
				<text class="text-lg">{{ privacyStatus }}</text>
			</view>
			<view class="mt-3">
				<button type="primary" @tap="getPrivacySetting">获取隐私协议授权状态</button>
				<button type="primary" class="mt-3" open-type="agreePrivacyAuthorization">同意隐私协议</button>
				<button type="primary" class="mt-3" @tap="resetPrivacyAuthorization">重置隐私协议</button>
				<button type="primary" class="mt-3" @tap="openPrivacyDialog">显示隐私政策弹窗</button>
			</view>
		</view>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script setup>
	// #ifdef APP
	import { usePrivacy } from '@/uni_modules/kux-apiuse-privacy';
	
	const privacyStatus = ref('未获取')
	
	const { appPrivacy, getPrivacySetting, onPrivacyAuthorizationChange, offPrivacyAuthorizationChange, resetPrivacyAuthorization } = usePrivacy();
	
	const id = onPrivacyAuthorizationChange((res) => {
		privacyStatus.value = res.needAuthorization ? '未同意' : '已同意'
		const privacyState = "监听到隐私协议状态已变更为 " + privacyStatus.value;
		uni.showToast({
			"position": "bottom",
			"title": privacyState
		})
	})
	uni.showToast({
		"position": "bottom",
		"title": "开启监听隐私协议状态"
	})
	
	const openPrivacyDialog = () => {
		uni.openDialogPage({
			url: '/pages/privacy/privacyModal'
		})
	}
	
	watchEffect(() => {
		privacyStatus.value = appPrivacy.value == true ? '已同意' : (appPrivacy.value == null ? '未获取' : '未同意')
	})
	// #endif
</script>

<style>

</style>
```

+ 隐私弹窗页面

```html
<template>
  <view class="dialog-container">
    <view class="dialog-content">
      <view style="width: 100%;justify-content: center;align-items: center;background-color: #fff;">
        <image src="/static/logo.png" style="margin-top: 25px;width: 60px;height: 60px;"></image>
      </view>
      <text style="text-align: center;padding-top: 20px;font-size: 20px;color: black;background-color: #fff;"> 个人信息保护指引</text>
      <scroll-view  style="flex: 1;align-content: center;padding-top: 10px;padding-left: 25px;padding-right: 25px;background-color: #fff;"
        show-scrollbar="false">
        <rich-text style="font-size: 14px;color: red;" :nodes="htmlString" @itemclick="itemClick"></rich-text>
      </scroll-view>
      <button class="button" style="background-color: #fff;" @click="reject" hover-class="button-hover1">不同意</button>
      <button class="button"  style="background-color: royalblue;" hover-class="button-hover2" open-type="agreePrivacyAuthorization"
        @click="agree">同意</button>
    </view>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        number: 0,
        htmlString: '<span style="color:grey">欢迎使用kux生态插件，你可以通过<a href="https:www.baidu.com" >《用户服务协议》</a></strong>及相关个人信息处理规则帮助你了解我们如何收集、处理个人信息。根据《常见类型移动互联网应用程序必要个人信息范围规定》。同意《基本功能数据处理规则》仅代表你同意使用浏览、搜索、下载等主要功能收集、处理相关必要个人信息及数据。此外，为了向你提供更多丰富、优质的服务，我们可能在你使用本产品的过程中提供电商购物、财经支付等扩展功能，如果你拒绝提供，你可能无法使用相应的扩展服务，但不影响你使用我们的基础服务。我们通过<a href="https:www.baidu.com">《隐私政策》</a></strong>帮助你全面了解我们的服务及收集、处理个人信息的详细情况。</span>'
      }
    },
    unmounted() {
      uni.offPrivacyAuthorizationChange(this.number)
    },
    onLoad() {
      this.number = uni.onPrivacyAuthorizationChange((callback) => {
        uni.showToast({
          title: 'isPrivacyAgree:' + !callback.needAuthorization
        })
      })
    },
    methods: {
      agree() {
        uni.closeDialogPage({
          dialogPage: this.$page
        })
      },
      reject() {
        uni.resetPrivacyAuthorization()
        uni.closeDialogPage({
          dialogPage: this.$page
        })
      },
      itemClick(e : RichTextItemClickEvent) {
        let ref = e.detail.href
        if (ref != null) {
          openSchema(ref) // 这里需要实现打开url的方法，仅作演示
        }
      },
    }
  }
</script>
<style>
  .dialog-container {
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .dialog-content {
    height: 60%;
    width: 80%;
    /* background-color: #fff; */
    border-radius: 12px;
  }

  .mt-10 {
    margin-top: 10px;
  }

  .button {
    border: 0px;
    border-radius: 0px;
    font-size: 15px;
    color: lightgrey;
    text-align: center;
    line-height: 40px;
  }

  .button-hover1 {
   color:  black;
    background-color: #fff;
  }
  .button-hover2 {
   color: black;
    background-color: royalblue;
  }
</style>
```

> 注意
> 
> 该插件运行环境要求需要uni编译器版本在 `4.32` 及以上

### usePrivacy 返回参数说明
#### appPrivacy
+ 说明：当前隐私授权状态，默认为 `null`
+ 类型：`Ref<boolean | null>`

#### getPrivacySetting
+ 说明：获取隐私协议状态
+ 类型：`() => Promise<KuxGetPrivacySettingReturn>`

#### KuxGetPrivacySettingReturn的属性值
兼容性需要参考 [GetPrivacySettingSuccessResult](https://doc.dcloud.net.cn/uni-app-x/api/privacy.html#getprivacysettingsuccessresult-values)

| 名称 | 类型 | 描述
| --- | --- | ---
| needAuthorization | `boolean` | 是否需要用户授权隐私协议(用户之前同意过返回false，没同意过则返回true)
| privacyContractName | `string | null` | 隐私授权协议的名称
| errMsg | `string` | 
| fail | `any | null` |

#### resetPrivacyAuthorization
+ 说明：重置隐私协议状态为未同意，适用于隐私协议变更，需要重新同意的场景。
+ 类型：`() => void`

#### onPrivacyAuthorizationChange
+ 说明：开启监听隐私协议状态改变
+ 类型：`(callback: (res: PrivacyChangeResult) => void) => number`

#### PrivacyChangeResult 的属性值
参考 [PrivacyChangeResult 的属性值](https://doc.dcloud.net.cn/uni-app-x/api/privacy.html#privacychangeresult-values)

#### offPrivacyAuthorizationChange
+ 说明：取消监听隐私协议状态改变
+ 类型：`(id: number) => void`

## 类型定义

```ts
/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type KuxGetPrivacySettingReturn = {
	needAuthorization: boolean
	privacyContractName: string | null
	errMsg: string
	fail: any | null
}

export type KuxUsePrivacyReturn = {
	getPrivacySetting: () => Promise<KuxGetPrivacySettingReturn>
	resetPrivacyAuthorization: () => void
	onPrivacyAuthorizationChange: (callback: (res: PrivacyChangeResult) => void) => number
	offPrivacyAuthorizationChange: (id: number) => void
	appPrivacy: Ref<boolean | null>
}
```

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
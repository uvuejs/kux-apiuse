# kux-apiuse-loading
该插件在集成了 [uni.showLoading](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html) 的异步版本封装的同时，又新增了页面响应式变量方便控制全局自定义loading组件状态，使业务代码更简洁高效。

## 插件特色
+ 异步hooks函数风格
+ 简洁高效
+ loading响应式变量

## 用法演示
```html
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<view class="flex px-3 py-3">
			<text class="text-xl mb-2 font-bold">showLoading用法演示</text>
			<button type="primary" class="w-full mb-2" @tap="onTapShowLoading">显示 loading 提示框</button>
			<button type="default" class="w-full mb-2" @tap="hideLoading">隐藏 loading 提示框</button>
			<text class="text-xl mb-2 font-bold">页面loading变量切换用法演示</text>
			<text class="mb-2 text-lg">请求loading: {{ loading }}</text>
			<button type="primary" class="w-full mb-2" @tap="onTapMockRequest">开始模拟请求</button>
		</view>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script setup>
	import { useLoading } from '@/uni_modules/kux-apiuse-loading';
	
	const { loading, toggle, setLoading, showLoading, hideLoading } = useLoading(false);
	
	const request = async (): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve(true);
			}, 2000);
		})
	}
	
	const onTapShowLoading = async () => {
		const { success, fail } = await showLoading({
			title: ''
		} as ShowLoadingOptions)
		
		if (fail != null) {
			console.log('错误', fail);
			return
		}
		
		console.log(success);
		
		return
	}
	
	const onTapMockRequest = async () => {
		toggle()
		await request()
		toggle()
	}
	
	watchEffect(() => {
		if (loading.value) {
			showLoading({
				title: ''
			})
		} else {
			hideLoading()
		}
	})
</script>

<style>

</style>

```
> 注意
> 
> 该插件运行环境要求需要uni编译器版本在 `4.32` 及以上

## useLoading 返回参数说明
### loading
页面 loading 响应式变量, 方便控制页面全局的自定义loading组件状态

### toggle
用来切换 loading 响应式变量的值, 每次调用后 loading 会在 `true/false` 之间切换.

### setLoading
用来手动设置 loading 响应式变量的值

### showLoading
对应 [uni.showLoading](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html#showloading)

### hideLoading
对应 [uni.hideLoading](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html#hideloading)

## showLoading 返回参数说明
### success
对应 [uni.showLoading](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html#showloading) 的 `ShowLoadingSuccess`

#### fail
对应 [uni.showLoading](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html#showloading) 的 [IPromptError](https://doc.dcloud.net.cn/uni-app-x/api/show-loading.html#iprompterror-values)

## 类型定义
```ts
/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type KuxShowLoadingReturn = {
	success: ShowLoadingSuccess | null
	fail: ShowLoadingFail | null
}

export type KuxUseLoadingReturn = {
	setLoading: (loadingValue: boolean) => void
	toggle: () => void
	showLoading: (options: ShowLoadingOptions) => Promise<KuxShowLoadingReturn>
	hideLoading: () => void
	loading: Ref<boolean>
}

export declare function useLoading (initLoading: boolean): KuxUseLoadingReturn
```

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
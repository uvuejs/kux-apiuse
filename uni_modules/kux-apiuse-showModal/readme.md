# kux-apiuse-showModal
该插件是 [uni.showModal](https://doc.dcloud.net.cn/uni-app-x/api/show-modal.html) 的异步版本封装，在支持原生API所有特性前提下可以使用异步hooks函数实现业务逻辑，使业务代码更简洁高效。

## 插件特色
+ 异步hooks函数风格
+ 简洁高效

## 基本用法
```ts
import { useShowModal } from '@/uni_modules/kux-apiuse-showModal'
	
const { showModal } = useShowModal();
	
const modal = async (): Promise<void> => {
	const { success, fail } = await showModal({
		title: '提示',
		content: '这是弹窗内容'
	} as ShowModalOptions);
	
	if (fail != null) {
		console.log('错误', fail.errMsg);
		return;
	}
	
	if (success!.confirm == true) {
		console.log('点击了确定');
		return
	}
	
	console.log('点击了取消');
	return;
}
```
> 注意
> 
> 该插件运行环境要求需要uni编译器版本在 `4.32` 及以上

## showModal 返回参数说明
### success
对应 [uni.showModal](https://doc.dcloud.net.cn/uni-app-x/api/show-modal.html) 的 [ShowModalSuccess](https://doc.dcloud.net.cn/uni-app-x/api/show-modal.html#showmodalsuccess-values)

#### fail
对应 [uni.showModal](https://doc.dcloud.net.cn/uni-app-x/api/show-modal.html) 的 [IPromptError](https://doc.dcloud.net.cn/uni-app-x/api/show-modal.html#iprompterror-values)

## 类型定义
```ts
/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type KuxShowModalReturn = {
	success: ShowModalSuccess | null
	fail: IPromptError | null
}

export type KuxUseShowModalReturn = {
	showModal: (options: ShowModalOptions) => Promise<KuxShowModalReturn>
}

export declare function useShowModal (): KuxUseShowModalReturn
```

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手